# Jupyter book for few seminars on stratified turbulence

I (Pierre Augier) would like to prepare 3 seminars about stratified turbulence
for lectures to TMA students.

## How to build

Install [pdm](https://pdm.fming.dev) and run:

```sh
pdm install
pdm run build
```

`mdformat` can be installed with pipx with:

```sh
pipx install mdformat-myst --include-deps
```
