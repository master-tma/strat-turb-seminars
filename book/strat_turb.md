# Stratified turbulence

```{math}
\newcommand{\p}{\partial}
\newcommand{\vv}{\boldsymbol{v}}
\newcommand{\xx}{\boldsymbol{x}}
\newcommand{\bnabla}{\boldsymbol{\nabla}}
\newcommand{\Dt}{\text{D}_t}
\newcommand{\R}{\mathcal{R}}
\newcommand{\epsK}{\varepsilon_K}
```

```{role} raw-latex(raw)
---
format: latex
---
```

{raw-latex}`\newcommand{\p}{\partial}`
{raw-latex}`\newcommand{\vv}{\boldsymbol{v}}`
{raw-latex}`\newcommand{\xx}{\boldsymbol{x}}`
{raw-latex}`\newcommand{\bnabla}{\boldsymbol{\nabla}}`
{raw-latex}`\newcommand{\Dt}{\text{D}_t}`
{raw-latex}`\newcommand{\R}{\mathcal{R}}`
{raw-latex}`\newcommand{\epsK}{\varepsilon_K}`

## Few experimental results

```{figure} ./figs/fig_zigzag_instab_simple.png
---
width: 70%
name: fig_zigzag_instab_simple.png
---
(a) Photographies of the evolution of the zigzag instability of two columnar counter
rotating vortices. The dipole propagates initially towards the reader.
(b) Scheme of the non-linear evolution of the zigzag instability inllustrating the
creation of pancake vortices. Taken from {cite:t}`BillantChomaz2000experimental`.
```

```{figure} ./figs/fig_praud_2d.png
---
width: 65%
name: fig_praud_2d.png
---
Streak photographs of an evolving, grid-generated turbulent flow.
Taken from {cite:t}`PraudFinchamSommeria2005`.
```

```{figure} ./figs/fig_praud_3d.png
---
width: 60%
name: fig_praud_3d.png
---
Initial three-dimensional instability of the laminar wake represented in terms of
isosurfaces of the vertical component of vorticity $\omega_z$.
Taken from {cite:t}`PraudFinchamSommeria2005`.
```


```{figure} ./figs/fig_augier_exp_phd.png
---
width: 80%
name: fig_augier_exp_phd.png
---
Horizontal ((a), (c), (e)) and vertical ((b), (d), (f)) cross-sections of the velocity
in the quasi-stationary regime for three values of the buoyancy Reynolds number $\R$.
Taken from {cite:t}`Augier2014experimental`.
```


## Scaling analyses

### Inviscid stratified flows

Let us follow {cite:t}`BillantChomaz2001` by considering a flow with typical
horizontal velocity $U$, typical horizontal scale $L_h$ and typical vertical scale
$L_z$. We define the aspect ratio $\alpha \equiv L_z/L_h$. The most important
non-dimensional parameter is the horizontal Froude number
$F_h \equiv
U / (N L_h) $.

$$
\begin{align}
\Dt \vv_h & = - \bnabla_h p \Rightarrow p \sim U^2,\\
\Dt v_z & = - \p_z p  +  b  \Rightarrow ?,\\
\Dt b & = - N^2 v_z  \Rightarrow  b \sim  N^2 L_h  \frac{v_z}{U}.\\
\end{align}
$$

We found the first two scaling laws. We can then compute the ratio

$$ \frac{\Dt v_z}{b} \sim F_h^2, $$

which implies that the two terms of the right hand side of the vertical velocity
equation have to be of the same order, which gives:

$$
\frac{v_z}{U} \simeq \frac{{F_h}^2}{\alpha}
$$

We can rewrite the equation using the dimensionless variables:

$$
\begin{align}
\Dt \vv_h & = - \bnabla_h p,\\
{F_h}^2 \Dt v_z & = - \p_z p  +  b,\\
\Dt b & = - N^2 v_z,\\
\end{align}
$$

with

$$\Dt = \p_t + \vv_h \cdot \bnabla + \left(\frac{F_h}{\alpha}\right)^2 v_z \p_z. $$

Dominant balance gives $\alpha\sim F_h \Rightarrow L_z \sim U/N$. This important
scale is called the buoyancy scale $L_b \equiv U/N$. This scaling implies that the
potential energy is of the same order of magnitude as the kinetic energy.

### Viscous stratified flows

{cite:t}`Brethouwer+2007`

Using the strongly stratified scaling presented above, the diffusive equations are
written in non-dimensional form as

$$
\begin{align}
\Dt \vv_h & = - \bnabla_h p + \frac{1}{Re \alpha^2} (\p_{zz} + \alpha^2 \bnabla_h^2) \vv_h,\\
{F_h}^2 \Dt v_z & = - \p_z p + b + \frac{{F_h}^2}{Re \alpha^2} (\p_{zz} + \alpha^2 \bnabla_h^2) v_z,\\
\Dt b & = - N^2 v_z + \frac{1}{ScRe \alpha^2} (\p_{zz} + \alpha^2 \bnabla_h^2) b,\\
\bnabla_h \cdot \vv_h + \left(\frac{F_h}{\alpha}\right)^2 \p_z v_z & = 0,\\
\end{align}
$$

where $Sc = \nu / \kappa$ is the Schmidt number. If we only keep the dominant
terms when $F_h \rightarrow 0$, it yields

$$
\Dt \vv_h & = - \bnabla_h p + \frac{1}{Re \alpha^2} \p_{zz} \vv_h,\\
0 & = - \p_z p + b,\\
\Dt b & = - v_z + \frac{1}{ScRe \alpha^2} \p_{zz} b,\\
\bnabla_h \cdot \vv_h + \left(\frac{F_h}{\alpha}\right)^2 \p_z v_z & = 0.\\
$$

At this point, we need to define the buoyancy Reynolds number

$$\R = Re F_h^2 = \frac{\epsK}{\nu N^2} = \left(\frac{L_b}{L_\nu}\right)^2,$$

where $L_\nu = \sqrt{\nu L_h / U}$.

- Small buoyancy Reynolds number limit

$L_z \sim L_\nu$. Viscous stratified turbulence with 2D like advective term.
Recover equations found by {cite:t}`Riley+1981` in the limit of $Fr < 1$ (i.e.
$F_h < 1$ and $F_v < 1$).

$$
(\p_t + \vv_h \cdot \bnabla) \vv_h & = - \bnabla_h p + \p_{zz} \vv_h,\\
0 & = - \p_z p + b,\\
(\p_t + \vv_h \cdot \bnabla) b & = - v_z + \frac{1}{Sc} \p_{zz} b,\\
\bnabla_h \cdot \vv_h & = 0.\\
$$

- Large buoyancy Reynolds number limit

$$
(\p_t + \vv_h \cdot \bnabla + v_z \p_z) \vv_h & = - \bnabla_h p,\\
0 & = - \p_z p + b,\\
(\p_t + \vv_h \cdot \bnabla + v_z \p_z) b & = - v_z,\\
\bnabla_h \cdot \vv_h + \p_z v_z & = 0.\\
$$

$L_z \sim L_b = U/N$, $E_P \sim E_K$. Strongly anisotropic with a 3D like
advective term.

$\Rightarrow$ "Strongly Stratified Turbulence" regime, latter called LAST regime
(Layered Anisotropic Stratified Turbulence).

## Few numerical results

- {cite:t}`RileyDeBruynKops2003`
  [pdf](https://people.umass.edu/debk/Papers/riley03.pdf)

- {cite:t}`Lindborg2006`
  [pdf](https://www.researchgate.net/profile/Erik-Lindborg/publication/231939096_The_energy_cascade_in_a_strongly_stratified_fluid/links/640efb4ea1b72772e4f4417b/The-energy-cascade-in-a-strongly-stratified-fluid.pdf)

```{figure} ./figs/fig_table_Lindborg2006.png
---
width: 70%
name: fig_table_Lindborg2006
---
{cite:t}`Lindborg2006` simulation parameters. $No$ is the number of time steps
and $\Delta t$ is the time step in the statistically stationary state. For run
6 no stationary state was reached and the time step was therefore decreasing
during the whole run.
```

```{figure} ./figs/fig_Lindborg2006_spectra.png
---
width: 70%
name: fig_Lindborg2006_spectra
---
Compensated horizontal kinetic and potential energy spectra from runs 8 and 9.
```

- {cite:t}`HebertDeBruynKops2006`
  [pdf](https://people.umass.edu/debk/Papers/hebert06b.pdf)

- {cite:t}`Brethouwer+2007`
  [pdf](http://yakari.polytechnique.fr/Django-pub/documents/chomaz2007rp-1pp.pdf)

- http://legi.grenoble-inp.fr/people/Pierre.Augier/docs/2023/gafdem/pres.html
