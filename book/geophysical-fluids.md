```{math}
\newcommand{\p}{\partial}
\newcommand{\vv}{\boldsymbol{v}}
\newcommand{\xx}{\boldsymbol{x}}
\newcommand{\bnabla}{\boldsymbol{\nabla}}
\newcommand{\Dt}{\text{D}_t}
```

```{role} raw-latex(raw)
---
format: latex
---
```

{raw-latex}`\newcommand{\p}{\partial}`
{raw-latex}`\newcommand{\vv}{\boldsymbol{v}}`
{raw-latex}`\newcommand{\xx}{\boldsymbol{x}}`
{raw-latex}`\newcommand{\bnabla}{\boldsymbol{\nabla}}`
{raw-latex}`\newcommand{\Dt}{\text{D}_t}`

# Geophysical fluid mechanics

## Generalities on geophysical fluids

The Earth atmosphere, oceans and lakes are geophysical fluids sharing
similarities. They are all weakly viscous fluids at high Reynolds number
influenced by:

- density differences, usually density stratification even if thermal convection
  is important in some cases,

- the Earth rotation,

- forcing, through radiation or boundaries

- geometric contrains (thin layers, bassin, spherical, ...)

A branch of fluid mechanics is dedicated to study their dynamics.

Atmosphere: different layers and interfaces, troposphere, stratosphere and
mesosphere

```{figure} ./figs/fig_atmo.png
---
width: 70%
name: fig_atmo.png
---
The physical vertical structure of the atmosphere as a function of altitude.
Copyright University of Lagos (Source:
http://unilaggeography2012.blogspot.nl/p/gry-101-introduction-to-physical.html).
```

Oceans (and lakes): mixed layer close to the surface, pycnocline (typically
$N\simeq 5$ cph), stratified interior (typically $N\simeq 1$ cph), nonlinear
stratification leading to vertical modes.

## Navier-Stokes equations and primitive equations

### Boussinesq approximation

For a compressible fluid, the conservation of mass can be written as:

$${\text{D}_t}{\rho_{\text{tot}}}+ {\rho_{\text{tot}}}{\bnabla}\cdot {\boldsymbol{u}}= 0 \Leftrightarrow
{\partial}_t {\rho_{\text{tot}}}+ {\bnabla}\cdot({\rho_{\text{tot}}}{\boldsymbol{u}}) = 0.$$

The Navier-Stokes equation is

$${\rho_{\text{tot}}}{\text{D}_t}{\boldsymbol{u}}= -{\bnabla}\tilde p + {\rho_{\text{tot}}}{\boldsymbol{g}}+ {\rho_{\text{tot}}}\nu {\bnabla}^2 {\boldsymbol{u}}.$$

The density is decomposed in 3 parts:
${\rho_{\text{tot}}}= \rho_0 +
\tilde\rho(z) + \rho'$, where the time average
density is $\bar\rho(z) = \rho_0 + \tilde\rho(z)$.

The Boussinesq approximation consists in replacing ${\rho_{\text{tot}}}$ by
$\rho_0$ everywhere except in the buoyancy term.

$${\text{D}_t}{\boldsymbol{u}}= -{\bnabla}\frac{\tilde p}{\rho_0}
  + \frac{{\rho_{\text{tot}}}{\boldsymbol{g}}}{\rho_0}
  + \nu {\bnabla}^2 {\boldsymbol{u}}.
$$

and the mass conservation becomes ${\bnabla}\cdot {\boldsymbol{u}}=
0$.

We can subtract to this equation its static solution obtained for
${\boldsymbol{u}}= 0$:

$$0 = -{\bnabla}\frac{\tilde p_s}{\rho_0} + \frac{\bar\rho {\boldsymbol{g}}}{\rho_0}$$

which gives

$${\text{D}_t}{\boldsymbol{u}}= -{\bnabla}p + {\boldsymbol{b}}+ \nu {\bnabla}^2 {\boldsymbol{u}},$$

where $p$ is the rescaled dynamic pressure and ${\boldsymbol{b}}$ is the buoyancy.

The conservation of internal energy and mass of salt can be rewritten as

$${\text{D}_t}{\rho_{\text{tot}}}= \kappa {\bnabla}^2 {\rho_{\text{tot}}},$$

which leads with some assumption to

$${\text{D}_t}b + N^2 u_z = \kappa {\bnabla}^2 b,$$

where $N = \sqrt{d_z \bar b_{\text{tot}}}$ is the Brunt-Väisälä frequency.

### Linear inviscid equations and eigenmodes

The linear inviscid equations are

$$
\begin{align}
{\partial_t}{\boldsymbol{u}} & = -{\bnabla}p + b e_z,\\
{\partial_t}b & = - N^2 u_z.
\end{align}
$$

We want to obtain a wave equation, i.e. a linear differential equation for only
one variable.

To be able to eliminate the pressure, we know that we can take the divergence of
the velocity equation to obtain the linear Poisson equation:

$$ \bnabla^2 p = \p_z b $$

To eliminate the velocity, we can take the time derivative of the buoyancy
equation:

$$ {\partial_{tt}} b  = - N^2 ( - \p_z p + b ). $$

$$ {\partial_{tt}}\bnabla^2 b  = - N^2 ( - \p_z \bnabla^2 p + \bnabla^2b ). $$

$$ {\partial_{tt}}\bnabla^2 b  = - N^2 \bnabla_h^2 b. $$

Taking the time and space Fourier transforms, we obtain the dispersion relation:

$$ \omega^2 = N^2 \left(\frac{k_h}{k}\right)^2 = N^2 \sin^2\theta,$$

where $\theta$ is the angle between the wave vector and the verticale direction.

#### Few consequences of this uncommon dispersion relation:

- $0 < \omega < N$,

- slow waves for quasi vertical wave vector ($v_z \simeq 0$) and fast waves
  $\omega \simeq N$ for horizontal wave vector ($v_h \simeq 0$).

- ...

#### Rotational - toroidal modes

We can also take the vertical componant of the rotational of the velocity equation
to get the equation for the vertical vorticity $\p_t \omega_z = 0$.
