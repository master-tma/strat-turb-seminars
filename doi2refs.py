from pathlib import Path
import sys

from doi2bib.crossref import get_bib_from_doi


with open("DOIs-bib.txt", "r") as file:
    lines = file.readlines()


refs = []

new_lines = []

for line in lines:
    doi = line.split("#")[0].strip()
    if not doi:
        new_lines.append(line)
        continue

    print(doi)

    found, bib = get_bib_from_doi(doi)

    if not found:
        new_lines.append(line)
        continue

    refs.append(bib)


if not refs:
    sys.exit()

path_refs = Path("book/references.bib")

assert path_refs.exists()

refs = [ref.strip() for ref in refs]

with open(path_refs, "a") as file:
    file.write("\n\n" + "\n\n\n".join(refs))

with open("DOIs-bib.txt", "w") as file:
    file.write("".join(new_lines))
