
html: sync
	pdm run build

pdf: sync
	pdm run build-pdf

clean:
	pdm run clean

format:
	pdm format

install:
	pdm install

sync:
	pdm sync
